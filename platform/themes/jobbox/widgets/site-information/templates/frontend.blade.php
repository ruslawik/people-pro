<div class="footer-col-1 col-md-3 col-sm-12">
    <a href="{{ route('public.index') }}">
        <img
            alt="{{ setting('site_title') }}"
            src="{{ RvMedia::getImageUrl($config['logo'] ?: setting('theme-jobbox-logo')) }}"
        >
    </a>
    <div class="mt-20 mb-20 font-xs color-text-paragraph-2">
        {!! BaseHelper::clean($config['introduction']) !!}
    </div>
    <div class="footer-social">
        @foreach($config['socials'] as $social)
            @if($url = $config[$social . '_url'])
                <a class="icon-socials icon-{{ $social }}" href="{{ $url }}"></a>
            @endif
        @endforeach
    </div>
</div>
