<div class="footer-col-2 col-md-2 col-xs-6">
    <h6 class="mb-20">{!! BaseHelper::clean($config['name']) !!}</h6>
    {!!
        Menu::generateMenu([
            'slug'    => $config['menu_id'],
            'view'    => 'footer-menu',
            'options' => ['class' => 'menu-footer']
        ])
    !!}
</div>
